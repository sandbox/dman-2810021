Growl
=====

Provides the facility to post Drupal system messages to 
[Growl](http://growl.info).
Growl is an OSX (only) utility for displaying system notifications to a 
developers desktop.

When used with Drupal it can be configured to alert you to logged messages
and warnings in realtime.


REQUIREMENTS
------------

You must install Growl from 
[http://growl.info/downloads](http://growl.info/downloads)

For the most recent version, it costs _just a couple of $_,
 but the older versions are free, or you can even 
 [build it yourself](http://growl.info/documentation/developer/growl-source-install.php)
 if you really want to.

To get this working on your development machine, you **must also install** the
 optional 'growlnotify' commandline utility.
This must be downloaded separately from the page at
 http://growl.info/downloads#growlnotify .
By default this will install itself into /usr/local/bin .

To test it is installed, you can type

    growlnotify "Hello" -m "This is a message"


CONFIGURATION
-------------

When the module is installed on your Drupal site, 
 configurations are found at [/admin/config/development/logging](/admin/config/development/logging)

You can set the 'severity' log level there.

### Log level

On install, the growl 'severity' level is set to 'DEBUG'.
This will pick up all log messages, so it's very verbose.

To turn down the volume of messages, set the growl.settings:severity to a
lower level, as enumerated in the RfcLogLevel specification.

    drush config-set growl.settings severity 5 # NOTICE
    
Level 'NOTICE' (5) will ignore the lower 'INFO' and 'DEBUG' messages.

    drush config-set growl.settings severity 3 # ERROR
    
Level 'ERROR' (3) will also ignore the lower 'WARNING' and 'NOTICE' messages.
