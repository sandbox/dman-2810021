<?php

namespace Drupal\growl\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Sends log messages to the Growl API (to display on the developers machine).
 *
 * @package Drupal\growl\Logger
 */
class GrowlLog implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * A configuration object for my settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Constructs a Logger object.
   *
   * Makes handles on the configs and the log parser available.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser) {
    $this->config = $config_factory->get('growl.settings');
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = array()) {
    // $context is expected to contain values including :
    // $context['uid']
    // 'type' => Unicode::substr($context['channel'], 0, 64)
    // $context['link']
    // $context['request_uri']
    // $context['referer']
    // $context['ip']
    // $context['timestamp'].
    if ($level > $this->config->get('severity')) {
      // Early exit if the volume is turned down.
      return;
    }

    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

    // Create the Growl message indicating message severity and type.
    $levels = RfcLogLevel::getLevels();
    $level_label = $levels[$level];
    $label = $level_label . ": " . $context['channel'];

    $args = array();

    growl($message, $args, $label);
  }

}
